﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task2.Models;

namespace Task2.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [HttpGet]
        [Authorize(Roles ="admin")]
        public ActionResult AsignManagerToUser()
        {
            AssignManagerVM objvm = new AssignManagerVM();
            objvm.ManagerList = GetAll_Managers();
            objvm.Userlist = GetAll_Users();
            return View(objvm);
        }
        [NonAction]
        public List<SelectListItem> GetAll_Managers()
        {
            List<SelectListItem> listManager = new List<SelectListItem>();
            listManager.Add(new SelectListItem { Text = "Select", Value = "0" });
            List<AllroleandUser> users = Get_Username_And_Rolename();

            foreach (var item in users.Where(x => x.RoleName == "manager"))
            {
                listManager.Add(new SelectListItem { Text = item.UserName, Value = item.UserId.ToString() });
            }

            return listManager;
        }
        [NonAction]
        public List<SelectListItem> GetAll_Users()
        {
            List<SelectListItem> listuser = new List<SelectListItem>();
            listuser.Add(new SelectListItem { Text = "Select", Value = "0" });

            using (UsersContext db = new UsersContext())
            {
                foreach (var item in db.UserProfiles)
                {
                    listuser.Add(new SelectListItem { Text = item.UserName, Value = item.UserId.ToString() });
                }
            }
            return listuser;
        }
        [NonAction]
        public List<AllroleandUser> Get_Username_And_Rolename()
        {
            using (UsersContext db = new UsersContext())
            {
                var Alldata = (from User in db.UserProfiles
                               join WU in db.webpages_UsersInRole on User.UserId equals WU.UserId
                               join WR in db.Roles on WU.RoleId equals WR.RoleId
                               select new AllroleandUser { UserName = User.UserName, UserId = User.UserId, RoleName = WR.RoleName }).ToList();

                return Alldata;
            }
        }
        [NonAction]
        public List<AllManagerandUser> Get_Username_And_Managername()
        {
            using (UsersContext db = new UsersContext())
            {
                var Alldata = (from User in db.UserProfiles
                               join WU in db.UserProfiles on User.ManagerId equals WU.UserId.ToString()                              
                               select new AllManagerandUser { UserName = User.UserName, UserId = User.UserId, ManagerName = WU.UserName,ManagerId=WU.UserId }).ToList();

                return Alldata;
            }
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AsignManagerToUser(AssignManagerVM objvm)
        {
            if (objvm.ManagerId == "0")
            {
                ModelState.AddModelError("ManagerName", "Please select ManagerName");
            }
            if (objvm.UserID == "0")
            {
                ModelState.AddModelError("UserName", "Please select Username");
            }
            if (ModelState.IsValid)
            {


                using (UsersContext context = new UsersContext())
                {
                    int uid = Convert.ToInt32(objvm.UserID);
                    var User = context.UserProfiles.Where(x => x.UserId == uid).FirstOrDefault();
                    if (User != null)
                    {
                        if (Convert.ToInt32(User.ManagerId )> 0)
                        {
                            ModelState.AddModelError("ManagerName", "Manager " + GetUserName_BY_UserID(Convert.ToInt32(objvm.ManagerId)) + " is Allready assigned to user");
                        }
                        else
                        {
                            User.ManagerId = objvm.ManagerId;
                            context.Entry(User).State = EntityState.Modified;
                            context.SaveChanges();
                            ViewBag.ResultMessage = "User assigned to the Manager successfully !";
                        }
                    }
                }
                objvm.ManagerList = GetAll_Managers();
                objvm.Userlist = GetAll_Users();
                return View(objvm);
            }
            else
            {
                objvm.ManagerList = GetAll_Managers();
                objvm.Userlist = GetAll_Users();
            }
            return View(objvm);
        }
        [NonAction]
        public string GetUserName_BY_UserID(int UserId)
        {
            using (UsersContext context = new UsersContext())
            {
                var UserName = (from UP in context.UserProfiles
                                where UP.UserId == UserId
                                select UP.UserName).SingleOrDefault();
                return UserName;
            }
        }
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult RemoveManagerAssignedToUser()
        {
            AssignManagerVM objvm = new AssignManagerVM();
            objvm.ManagerList = GetAll_Managers();
            objvm.Userlist = GetAll_Users();
            return View(objvm);
        }
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveManagerAssignedToUser(AssignManagerVM objvm)
        {
            if (objvm.ManagerId == "0")
            {
                ModelState.AddModelError("ManagerName", "Please select ManagerName");
            }
            if (objvm.UserID == "0")
            {
                ModelState.AddModelError("UserName", "Please select Username");
            }
            if (ModelState.IsValid)
            {
                using (UsersContext context = new UsersContext())
                {
                    var User = context.UserProfiles.Where(x => x.UserId == Convert.ToInt32(objvm.UserID)).FirstOrDefault();
                    if (User != null)
                    {
                        if (Convert.ToInt32(User.ManagerId) > 0)
                        {
                            User.ManagerId = 0.ToString();
                            context.Entry(User).State = EntityState.Modified;
                            context.SaveChanges();
                            ViewBag.ResultMessage = "Manager was unassigned successfully !";
                        }
                        else
                        {
                            ViewBag.ResultMessage = "This user doesn't belong to selected Manager.";
                        }
                        objvm.ManagerList = GetAll_Managers();
                        objvm.Userlist = GetAll_Users();
                    }
                }
            }
            else
            {
                objvm.ManagerList = GetAll_Managers();
                objvm.Userlist = GetAll_Users();
            }
            return View(objvm);
        }
        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult DisplayAllUserManager()
        {
            AllManagerandUser objru = new AllManagerandUser();
            objru.AllDetailsUserlist = Get_Username_And_Managername();
            return View(objru);
        }
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Administrate()
        {
                return View();

        }
    }
}