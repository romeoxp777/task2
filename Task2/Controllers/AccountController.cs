﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Task2.Filters;
using Task2.Models;
using WebMatrix.WebData;

namespace Task2.Controllers
{
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Signup()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Signup(UserProfile model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {

                using (UsersContext db = new UsersContext())
                {
                    string username = model.UserName;
                    string password = model.Password;
                    // Now if our password was enctypted or hashed we would have done the
                    // same operation on the user entered password here, But for now
                    // since the password is in plain text lets just authenticate directly
                    bool userExist = db.UserProfiles.Any(user => user.UserName == username && user.Password == password);

                    // User found in the database
                    if (!userExist)
                    {
                        WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                        WebSecurity.Login(model.UserName, model.Password);
                        FormsAuthentication.SetAuthCookie(username, false);
                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "The user name is allready exist.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [HttpPost]
        public ActionResult Login(UserProfile model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {

                using (UsersContext db = new UsersContext())
                {
                    string username = model.UserName;
                    string password = model.Password;
                    try
                    {
                        WebSecurity.Login(model.UserName, model.Password);
                            FormsAuthentication.SetAuthCookie(username, false);
                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    catch
                    {
                        ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    }
                    
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult RoleAddToUser()
        {
            AssignRoleVM objvm = new AssignRoleVM();
            objvm.RolesList = GetAll_Roles();
            objvm.Userlist = GetAll_Users();
            return View(objvm);
        }
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult RoleAddToUser(AssignRoleVM objvm)
        {
            if (objvm.RoleName == "0")
            {
                ModelState.AddModelError("RoleName", "Please select RoleName");
            }
            if (objvm.UserID == "0")
            {
                ModelState.AddModelError("UserName", "Please select Username");
            }
            if (ModelState.IsValid)
            {
                if (Get_CheckUserRoles(Convert.ToInt32(objvm.UserID)) == true)
                {
                    ViewBag.ResultMessage = "This user already has the role specified !";
                }
                else
                {
                    var UserName = GetUserName_BY_UserID(Convert.ToInt32(objvm.UserID));
                    Roles.AddUserToRole(UserName, objvm.RoleName);
                    ViewBag.ResultMessage = "Username added to the role successfully !";
                }
                objvm.RolesList = GetAll_Roles();
                objvm.Userlist = GetAll_Users();
                return View(objvm);
            }
            else
            {
                objvm.RolesList = GetAll_Roles();
                objvm.Userlist = GetAll_Users();
            }
            return View(objvm);
        }
        [NonAction]
        public List<SelectListItem> GetAll_Roles()
        {
            List<SelectListItem> listrole = new List<SelectListItem>();
            listrole.Add(new SelectListItem { Text = "select", Value = "0" });
            using (UsersContext db = new UsersContext())
            {
                foreach (var item in db.Roles)
                {
                    listrole.Add(new SelectListItem { Text = item.RoleName, Value = item.RoleName });
                }
            }
            return listrole;
        }
        [NonAction]
        public List<SelectListItem> GetAll_Users()
        {
            List<SelectListItem> listuser = new List<SelectListItem>();
            listuser.Add(new SelectListItem { Text = "Select", Value = "0" });

            using (UsersContext db = new UsersContext())
            {
                foreach (var item in db.UserProfiles)
                {
                    listuser.Add(new SelectListItem { Text = item.UserName, Value = item.UserId.ToString() });
                }
            }
            return listuser;
        }
        [NonAction]
        public bool Get_CheckUserRoles(int UserId)
        {
            using (UsersContext context = new UsersContext())
            {
                var data = (from WR in context.webpages_UsersInRole
                            join R in context.Roles on WR.RoleId equals R.RoleId
                            where WR.UserId == UserId
                            orderby R.RoleId
                            select new
                            {
                                WR.UserId
                            }).Count();

                if (data > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        [NonAction]
        public string GetUserName_BY_UserID(int UserId)
        {
            using (UsersContext context = new UsersContext())
            {
                var UserName = (from UP in context.UserProfiles
                                where UP.UserId == UserId
                                select UP.UserName).SingleOrDefault();
                return UserName;
            }
        }
        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult DisplayAllUserroles()
        {
            AllroleandUser objru = new AllroleandUser();
            objru.AllDetailsUserlist = Get_Username_And_Rolename();
            return View(objru);
        }
        [NonAction]
        public List<AllroleandUser> Get_Username_And_Rolename()
        {
            using (UsersContext db = new UsersContext())
            {
                var Alldata = (from User in db.UserProfiles
                               join WU in db.webpages_UsersInRole on User.UserId equals WU.UserId
                               join WR in db.Roles on WU.RoleId equals WR.RoleId
                               select new AllroleandUser { UserName = User.UserName, RoleName = WR.RoleName }).ToList();

                return Alldata;
            }
        }
        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult RemoveRoleAddedToUser()
        {
            AssignRoleVM objvm = new AssignRoleVM();
            objvm.RolesList = GetAll_Roles();
            objvm.Userlist = GetAll_Users();
            return View(objvm);
        }
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveRoleAddedToUser(AssignRoleVM objvm)
        {
            if (objvm.RoleName == "0")
            {
                ModelState.AddModelError("RoleName", "Please select RoleName");
            }
            if (objvm.UserID == "0")
            {
                ModelState.AddModelError("UserName", "Please select Username");
            }
            if (ModelState.IsValid)
            {
                if (Get_CheckUserRoles(Convert.ToInt32(objvm.UserID)) == true)
                {
                    var UserName = GetUserName_BY_UserID(Convert.ToInt32(objvm.UserID));
                    Roles.RemoveUserFromRole(UserName, objvm.RoleName);
                    ViewBag.ResultMessage = "Role removed from this user successfully !";
                }
                else
                {
                    ViewBag.ResultMessage = "This user doesn't belong to selected role.";
                }
                objvm.RolesList = GetAll_Roles();
                objvm.Userlist = GetAll_Users();
            }
            else
            {
                objvm.RolesList = GetAll_Roles();
                objvm.Userlist = GetAll_Users();
            }
            return View(objvm);
        }
        
    }
}