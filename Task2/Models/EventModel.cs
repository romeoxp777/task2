﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task2.Models
{
    public class EventModel
    {
        [Key]
        public int EventId { get; set; }
        public DateTime TimeOfStart { get; set; }
        public DateTime TimeOfEnd { get; set; }
        public int UserId { get; set; }


    }
    public class EventVM
    {

    }
}