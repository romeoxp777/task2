﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task2.Models
{
    public class AssignManagerVM
    {
        [Required(ErrorMessage = " Select Manager Name")]
        public string ManagerId { get; set; }
        [Required(ErrorMessage = "Select UserName")]
        public string UserID { get; set; }
        public List<SelectListItem> Userlist { get; set; }
        public List<SelectListItem> ManagerList { get; set; }

    }
    public class AllManagerandUser
    {
        public string ManagerName { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public int ManagerId { get; set; }
        public IEnumerable<AllManagerandUser> AllDetailsUserlist { get; set; }
    }
}